!IF "$(PLATFORM)"=="X64" || "$(PLATFORM)"=="x64"
ARCH=amd64
!ELSE
ARCH=x86
!ENDIF

OUTDIR=bin\$(ARCH)
OBJDIR=obj\$(ARCH)
SRCDIR=src
CHAKRACORE_SRC_DIR=D:\src\ChakraCore
GTEST_SRC_DIR=D:\src\googletest
GTEST_BUILD_DIR=D:\src\googletest\build\$(ARCH)

CC=cl
RD=rd/s/q
RM=del/q
LINKER=link
NASM=D:\nasm\latest\nasm.exe
TARGET=t.exe
SENDER_TARGET=sender.exe
TEST_TARGET=runtests.exe

OBJS=\
	$(OBJDIR)\main.obj\
	$(OBJDIR)\mainwindow.obj\
	$(OBJDIR)\uaf.obj\
	$(OBJDIR)\args.obj\
	$(OBJDIR)\chakra_client.obj\
	$(OBJDIR)\gadget.obj\

SENDER_OBJS=\
	$(OBJDIR)\utils.obj\
	$(OBJDIR)\sender.obj\

TEST_OBJS=\
	$(OBJDIR)\utils.obj\
	$(OBJDIR)\args.obj\

LIBS=\
	user32.lib\
	comctl32.lib\
	ole32.lib\
	gdi32.lib\
	chakracore.lib\
	gtest.lib\

TEST_LIBS=\
	user32.lib\
	gmock.lib\
	gtest_main.lib\

SENDER_LIBS=\
	user32.lib\
	gtest.lib\

CFLAGS=\
	/nologo\
	/c\
	/O1\
	/W4\
	/Zi\
	/EHsc\
	/D_CRT_SECURE_NO_WARNINGS\
	/Fo"$(OBJDIR)\\"\
	/Fd"$(OBJDIR)\\"\
	/I"$(CHAKRACORE_SRC_DIR)\lib\Jsrt"\
	/I"$(GTEST_SRC_DIR)\googletest\include"\
	/I"$(GTEST_SRC_DIR)\googlemock\include"\

LFLAGS=\
	/NOLOGO\
	/DEBUG\
	/SUBSYSTEM:WINDOWS\
!IF "$(ARCH)"=="x86"
	/LIBPATH:"$(CHAKRACORE_SRC_DIR)\Build\VcBuild\bin\x86_debug"\
!ELSE
	/LIBPATH:"$(CHAKRACORE_SRC_DIR)\Build\VcBuild\bin\x64_debug"\
!ENDIF
	/LIBPATH:"$(GTEST_BUILD_DIR)\lib\Release"\

TEST_LFLAGS=\
	/NOLOGO\
	/DEBUG\
	/SUBSYSTEM:CONSOLE\
	/LIBPATH:"$(GTEST_BUILD_DIR)\lib\Release"\

SENDER_LFLAGS=\
	/NOLOGO\
	/DEBUG\
	/SUBSYSTEM:CONSOLE\
	/LIBPATH:"$(GTEST_BUILD_DIR)\lib\Release"\

AFLAGS=\
	-g\
	-O0\
!IF "$(ARCH)"=="x86"
	-fwin32\
!ELSE
	-fwin64\
!ENDIF

all: $(OUTDIR)\$(TARGET) $(OUTDIR)\$(TEST_TARGET) $(OUTDIR)\$(SENDER_TARGET)

$(OUTDIR)\$(TARGET): $(OBJS)
	@if not exist $(OUTDIR) mkdir $(OUTDIR)
	$(LINKER) $(LFLAGS) $(LIBS) /PDB:"$(@R).pdb" /OUT:$@ $**

{$(SRCDIR)}.cpp{$(OBJDIR)}.obj:
	@if not exist $(OBJDIR) mkdir $(OBJDIR)
	$(CC) $(CFLAGS) $<

{$(SRCDIR)}.asm{$(OBJDIR)}.obj:
	@if not exist $(OBJDIR) mkdir $(OBJDIR)
	$(NASM) $(AFLAGS) -o $@ $<

$(OUTDIR)\$(TEST_TARGET): $(TEST_OBJS)
	@if not exist $(OUTDIR) mkdir $(OUTDIR)
	$(LINKER) $(TEST_LFLAGS) $(TEST_LIBS) /PDB:"$(@R).pdb" /OUT:$@ $**

$(OUTDIR)\$(SENDER_TARGET): $(SENDER_OBJS)
	@if not exist $(OUTDIR) mkdir $(OUTDIR)
	$(LINKER) $(SENDER_LFLAGS) $(SENDER_LIBS) /PDB:"$(@R).pdb" /OUT:$@ $**

clean:
	@if exist $(OBJDIR) $(RD) $(OBJDIR)
	@if exist $(OUTDIR)\$(TARGET) $(RM) $(OUTDIR)\$(TARGET)
	@if exist $(OUTDIR)\$(TARGET:exe=ilk) $(RM) $(OUTDIR)\$(TARGET:exe=ilk)
	@if exist $(OUTDIR)\$(TARGET:exe=pdb) $(RM) $(OUTDIR)\$(TARGET:exe=pdb)
	@if exist $(OUTDIR)\$(TEST_TARGET) $(RM) $(OUTDIR)\$(TEST_TARGET)
	@if exist $(OUTDIR)\$(TEST_TARGET:exe=ilk) $(RM) $(OUTDIR)\$(TEST_TARGET:exe=ilk)
	@if exist $(OUTDIR)\$(TEST_TARGET:exe=pdb) $(RM) $(OUTDIR)\$(TEST_TARGET:exe=pdb)
	@if exist $(OUTDIR)\$(SENDER_TARGET) $(RM) $(OUTDIR)\$(SENDER_TARGET)
	@if exist $(OUTDIR)\$(SENDER_TARGET:exe=ilk) $(RM) $(OUTDIR)\$(SENDER_TARGET:exe=ilk)
	@if exist $(OUTDIR)\$(SENDER_TARGET:exe=pdb) $(RM) $(OUTDIR)\$(SENDER_TARGET:exe=pdb)
