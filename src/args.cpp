#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <vector>
#include <string>

std::vector<std::string> get_args(const char *args) {
  std::vector<std::string> string_array;
  const char *prev, *p;
  prev = p = args;
  while (*p) {
    if (*p == ' ') {
      if (p > prev)
        string_array.emplace_back(args, prev - args, p - prev);
      prev = p + 1;
    }
    ++p;
  }
  if (p > prev)
    string_array.emplace_back(args, prev - args, p - prev);
  return string_array;
}

TEST(T, get_args) {
  EXPECT_THAT(get_args("1 2 3"), ::testing::ElementsAre("1", "2", "3"));
  EXPECT_THAT(get_args("  1   2 "), ::testing::ElementsAre("1", "2"));
  EXPECT_THAT(get_args("1"), ::testing::ElementsAre("1"));
  EXPECT_THAT(get_args(""), ::testing::ElementsAre());
  EXPECT_THAT(get_args("   "), ::testing::ElementsAre());
}