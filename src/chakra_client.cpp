#include <string>
#include <chakracore.h>

void Log(const char *format, ...);

std::wstring to_wstring(const char *s) {
  std::wstring w;
  int n = MultiByteToWideChar(CP_ACP,
                              /*dwFlags*/0,
                              s,
                              /*cbMultiByte*/-1,
                              nullptr,
                              0);
  if (auto p = new wchar_t[n]) {
    MultiByteToWideChar(CP_ACP,
                        /*dwFlags*/0,
                        s,
                        /*cbMultiByte*/-1,
                        p,
                        n);
    w = p;
    delete [] p;
  }
  return w;
}

// Cheatsheet:
// ChakraCore!Memory::HeapAllocator::AllocT<0>
// ChakraCore!Js::JavascriptString::RepeatCore
// ChakraCore!Js::TypedArray<unsigned int,0,0>::BaseTypedDirectSetItem
// .for (r @$t1=0; @$t1 < 0x10; r @$t1=@$t1+1) {s-d @$t1 * 10000000 l4000000 deadbeef}

// https://github.com/Microsoft/ChakraCore/wiki/Embedding-ChakraCore
void run_js(const char *code) {
  JsConfigure(0);

  JsValueRef result;
  JsSourceContext currentSourceContext = 0;

  JsRuntimeHandle runtime;
  JsCreateRuntime(JsRuntimeAttributeNone, nullptr, &runtime);

  JsContextRef context;
  JsCreateContext(runtime, &context);

  JsSetCurrentContext(context);

  std::wstring script = to_wstring(code);
  JsRunScript(script.c_str(), currentSourceContext++, /*sourceUrl*/L"", &result);

#if 0
  JsValueRef resultJSString;
  JsConvertValueToString(result, &resultJSString);
  const wchar_t *resultWC;
  size_t stringLength;
  JsStringToPointer(resultJSString, &resultWC, &stringLength);
  Log("Chakra returned %d bytes.\n", stringLength);
#endif

  JsSetCurrentContext(JS_INVALID_REFERENCE);
  JsDisposeRuntime(runtime);
}
