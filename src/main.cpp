#include <windows.h>
#include <vector>
#include "config.h"

std::vector<std::string> get_args(const char *args);
void UIThreadMain(const config &cfg);

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR cmdline, int) {
  const auto args = get_args(cmdline);
  config cfg;
  for (const auto &s : args) {
    if (!cfg.use_uaf && s == "--e") cfg.use_uaf = true;
    if (!cfg.use_chakra && s == "--js") cfg.use_chakra = true;
  }
  UIThreadMain(cfg);
  return 0;
}
