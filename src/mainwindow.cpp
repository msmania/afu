#include <vector>
#include <memory>
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include "basewindow.h"
#include "config.h"
#include "uaf.h"

const char SAMPLE_JS[] =
  "(()=>{\r\n"
  "  const factory = (array)=>{\r\n"
  "    let array_ = array;\r\n"
  "    let index_ = 0;\r\n"
  "    return ()=>{\r\n"
  "      '0'.repeat(1);\r\n"
  "      array_[index_] = 0xbad00000 + index_;\r\n"
  "      index_ = (index_ + 1) % array_.length\r\n"
  "      return {a:array_, b:index_};\r\n"
  "    };\r\n"
  "  };\r\n"
  "  const f1 = factory(new Uint32Array([\r\n"
  "    0xdeadbeea,\r\n"
  "    0xdeadbeeb,\r\n"
  "    0xdeadbeec,\r\n"
  "    0xdeadbeed,\r\n"
  "    0xdeadbeee,\r\n"
  "    0xdeadbeef,\r\n"
  "  ]));\r\n"
  "  for (let i = 0; i < 10; ++i) f1();\r\n"
  "  '0'.repeat(2);\r\n"
  "})()\r\n";

void Log(const char *format, ...) {
  char linebuf[1024];
  va_list v;
  va_start(v, format);
  vsprintf(linebuf, format, v);
  va_end(v);
  OutputDebugString(linebuf);
}

class MainWindow : public BaseWindow<MainWindow> {
  enum : int {
    IDC_BUTTON_ALLOC = 100,
    IDC_BUTTON_USE,
    IDC_BUTTON_FREE,
    IDC_BUTTON_EXPLOIT,
  };

  static constexpr int BUTTON_HEIGHT = 35;
  static constexpr int BUTTON_WIDTH = 60;
  static constexpr int PADDING = 5;

  static LRESULT CALLBACK SubProc(HWND wnd,
                                  UINT msg,
                                  WPARAM w,
                                  LPARAM l,
                                  UINT_PTR uIdSubclass,
                                  DWORD_PTR ref) {
    if (auto p = reinterpret_cast<MainWindow*>(ref))
      return p->SubProcInternal(wnd, msg, w, l, uIdSubclass);
    else
      return DefSubclassProc(wnd, msg, w, l);
  }

  HWND button1_{},
       button2_{},
       button3_{},
       button4_{},
       edit_{};
  HFONT monospace_{};
  std::unique_ptr<attack_base> attack_pack_;

  LRESULT CALLBACK SubProcInternal(HWND wnd,
                                   UINT msg,
                                   WPARAM w,
                                   LPARAM l,
                                   UINT_PTR) {
    if (msg == WM_KEYDOWN
        && w == 'A'
        && ((GetKeyState(VK_LCONTROL) & 0x8000)
            || (GetKeyState(VK_RCONTROL) & 0x8000))) {
      Edit_SetSel(wnd, 0, -1);
      return 0;
    }
    return DefSubclassProc(wnd, msg, w, l);
  }

  static void InitButton(HWND &handle,
                         LPCTSTR caption,
                         DWORD style,
                         int x, int y,
                         int width, int height,
                         HWND parent,
                         UINT id) {
    if (handle) DestroyWindow(handle);
    handle = CreateWindow("Button",
                          caption,
                          style,
                          x, y, width, height,
                          parent,
                          reinterpret_cast<HMENU>(id),
                          GetModuleHandle(nullptr),
                          /*lpParam*/nullptr);
  }

  bool InitWindow() {
    if (edit_) DestroyWindow(edit_);
    if (monospace_) DeleteObject(edit_);

    constexpr DWORD buttonStyle = WS_VISIBLE | WS_CHILD | BS_FLAT;
    InitButton(button1_ , "Alloc", buttonStyle,
               0, 0, BUTTON_WIDTH, BUTTON_HEIGHT,
               hwnd(), IDC_BUTTON_ALLOC);
    InitButton(button2_ , "Use", buttonStyle,
               BUTTON_WIDTH + PADDING, 0, BUTTON_WIDTH, BUTTON_HEIGHT,
               hwnd(), IDC_BUTTON_USE);
    InitButton(button3_ , "Free", buttonStyle,
               (BUTTON_WIDTH + PADDING) * 2, 0, BUTTON_WIDTH, BUTTON_HEIGHT,
               hwnd(), IDC_BUTTON_FREE);
    InitButton(button4_ , "Exploit", buttonStyle,
               (BUTTON_WIDTH + PADDING) * 3, 0, BUTTON_WIDTH, BUTTON_HEIGHT,
               hwnd(), IDC_BUTTON_EXPLOIT);

    constexpr DWORD editStyle = WS_VISIBLE | WS_CHILD | WS_BORDER
      | ES_AUTOHSCROLL | ES_AUTOVSCROLL | ES_MULTILINE;
    edit_ = CreateWindow("Edit",
                         SAMPLE_JS,
                         editStyle,
                         0, BUTTON_HEIGHT + PADDING,
                         BUTTON_WIDTH, BUTTON_HEIGHT,
                         hwnd(),
                         /*hMenu*/nullptr,
                         GetModuleHandle(nullptr),
                         /*lpParam*/nullptr);
    Log("Edit: %08x\n", edit_);
    SetWindowSubclass(edit_, SubProc, 0, reinterpret_cast<DWORD_PTR>(this));

    LOGFONT font{};
    font.lfHeight = -14;
    strcpy(font.lfFaceName, "Consolas");
    monospace_ = CreateFontIndirect(&font);
    PostMessage(edit_, WM_SETFONT, reinterpret_cast<WPARAM>(monospace_), 0);

    return button1_
           && button2_
           && button3_
           && button4_
           && edit_
           && monospace_;
  }

  void Resize() {
    RECT clientArea;
    GetClientRect(hwnd(), &clientArea);
    const int height = clientArea.bottom - clientArea.top;
    MoveWindow(edit_,
               0, BUTTON_HEIGHT + PADDING,
               clientArea.right - clientArea.left,
               clientArea.bottom - clientArea.top - BUTTON_HEIGHT - PADDING,
               /*bRepaint*/TRUE);
  }

public:
  MainWindow(attack_base *pack)
    : attack_pack_(pack)
  {}

  LPCTSTR ClassName() const {
    return "MainWindow";
  }

  LRESULT HandleMessage(UINT msg, WPARAM w, LPARAM l) {
    LRESULT ret = 0;
    switch (msg) {
    case WM_CREATE:
      if (!InitWindow()) {
        return -1;
      }
      break;
    case WM_SIZE:
      Resize();
      break;
    case WM_DESTROY:
      PostQuitMessage(0);
      break;
    case WM_COMMAND:
      switch (LOWORD(w)) {
      case IDC_BUTTON_ALLOC:
        attack_pack_->Alloc();
        break;
      case IDC_BUTTON_USE:
        attack_pack_->Use();
        break;
      case IDC_BUTTON_FREE:
        attack_pack_->Free();
        break;
      case IDC_BUTTON_EXPLOIT:
        attack_pack_->Exploit(edit_);
        break;
      }
      break;
    default:
      ret = DefWindowProc(hwnd(), msg, w, l);
      break;
    }
    return ret;
  }
};

attack_base *CreateAttackPack(const config &cfg);

void UIThreadMain(const config &cfg) {
  const auto flags = COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE;
  if (SUCCEEDED(CoInitializeEx(nullptr, flags))) {
    if (auto p = std::make_unique<MainWindow>(CreateAttackPack(cfg))) {
      if (p->Create("Welcome to the Jungle",
                    WS_OVERLAPPEDWINDOW,
                    /*style_ex*/0,
                    CW_USEDEFAULT, 0,
                    486, 300)) {
        ShowWindow(p->hwnd(), SW_SHOW);
        MSG msg{};
        while (GetMessage(&msg, nullptr, 0, 0)) {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
        }
      }
    }
    CoUninitialize();
  }
}