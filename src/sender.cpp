#include <vector>
#include <iostream>
#include <windows.h>

uint64_t hex_to_uint64(const char *s);
template <typename T>
std::vector<T> hexstr_to_blob(const char *s);

int main(int argc, char **argv) {
  if (argc >= 3) {
    const auto handle_as_int = hex_to_uint64(argv[1]);
    const auto blob = hexstr_to_blob<uint8_t>(argv[2]);
    HWND handle = reinterpret_cast<HWND>(handle_as_int & 0xffffffff);
    UINT msg = WM_SETTEXT;
    WPARAM w = 0;
    LPARAM l = reinterpret_cast<LPARAM>(blob.data());
    std::cout
      << "hwnd    = 0x" << handle << std::endl
      << "message = " << msg << std::endl
      << "w       = " << w << std::endl
      << "l       = 0x" << std::hex << l << std::endl
      << "--> " << SendMessage(handle, msg, w, l) << std::endl;
  }
  return 0;
}