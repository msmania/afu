#pragma once

struct base {
  base();
  virtual ~base();
  virtual void f1() {}
  virtual void f2() {}
  virtual void f3() {}
  virtual void f4() {}
  virtual void f5() {}
  virtual void f6() {}
  virtual void f7() {}
  virtual void f8() {}
  virtual uint32_t Abrakadabra(uint32_t *param) = 0;
};

class attack_base {
  static DWORD WINAPI StartWorker(LPVOID p);

  HWND input_{};
  const bool use_chakra_ = false;

  void Worker();

protected:
  const int alloc_size_ = 0x28;
  base *heart_{};

public:
  attack_base(bool use_chakra);
  virtual void Alloc() = 0;
  virtual void Use();
  virtual void Free() = 0;
  virtual void Exploit(HWND control);
};
