#include <gtest/gtest.h>
#include <gmock/gmock.h>

int htoc(char c) {
  return
    (c >= '0' && c <= '9') ? c - '0'
    : (c >= 'a' && c <= 'f') ? c - 'a' + 10
    :  (c >= 'A' && c <= 'F') ? c - 'A' + 10 : -1;
};

uint64_t hex_to_uint64(const char *s) {
  uint64_t ret = 0;
  const char *p = s;
  uint32_t valid_chars = 0;
  for (; *p && valid_chars <= 16; ++p) {
    if (p == s + 1
        && s[1] == 'x'
        && s[0] == '0') {
      valid_chars = 0;
      ret = 0;
    }
    else if (*p != '`') {
      int c = htoc(*p);
      if (c < 0) return 0;
      ret = (ret << 4) | c;
      if (ret > 0) ++valid_chars;
    }
  }
  return valid_chars <= 16 ? ret : 0;
}

TEST(T, hex_to_uint64) {
  EXPECT_EQ(hex_to_uint64("a123ffff"), 0xa123ffff);
  EXPECT_EQ(hex_to_uint64("0xa123ffff"), 0xa123ffff);
  EXPECT_EQ(hex_to_uint64("0xa123ffff-"), 0);
  EXPECT_EQ(hex_to_uint64("7ffe`72a38000"), 0x7ffe72a38000ull);
  EXPECT_EQ(hex_to_uint64("0x7ffe`72a38000"), 0x7ffe72a38000ull);
  EXPECT_EQ(hex_to_uint64("0x7ffe?72a38000"), 0);

  EXPECT_EQ(hex_to_uint64("0xffffffff`fffffffe"), 0xfffffffffffffffeull);
  EXPECT_EQ(hex_to_uint64("0xFFFFFFFFFFFFFFFD"), 0xfffffffffffffffdull);
  EXPECT_EQ(hex_to_uint64("ffffffff`fffffffc"), 0xfffffffffffffffcull);
  EXPECT_EQ(hex_to_uint64("FFFFFFFFFFFFFFFB"), 0xfffffffffffffffbull);
  EXPECT_EQ(hex_to_uint64("0xfffffffa"), 0xfffffffa);
  EXPECT_EQ(hex_to_uint64("FFFFFFF9"), 0xfffffff9);

  EXPECT_EQ(hex_to_uint64("0ffffffffffffffff"), 0xffffffffffffffffull);
  EXPECT_EQ(hex_to_uint64("0x0000000000000000ffffffffffffffff"), 0xffffffffffffffffull);
  EXPECT_EQ(hex_to_uint64("1ffffffffffffffff"), 0);
}

template <typename T>
std::vector<T> hexstr_to_blob(const char *s) {
  std::vector<T> blob;
  const char *p = s;
  int h = -1;
  for (; *p; ++p) {
    int c = htoc(*p);
    if (c < 0) return {};
    if (h == -1)
      h = c;
    else {
      blob.push_back(static_cast<T>((h << 4) | c));
      h = -1;
    }
  }
  return blob;
}

template std::vector<uint8_t> hexstr_to_blob(const char *s);

TEST(T, hexstr_to_blob) {
  EXPECT_THAT(hexstr_to_blob<int>("000000"), ::testing::ElementsAre(0, 0, 0));
  EXPECT_THAT(hexstr_to_blob<int>("a123ffff"), ::testing::ElementsAre(0xa1, 0x23, 0xff, 0xff));
  EXPECT_THAT(hexstr_to_blob<int>("a"), ::testing::ElementsAre());
  EXPECT_THAT(hexstr_to_blob<int>("A0"), ::testing::ElementsAre(0xa0));
  EXPECT_THAT(hexstr_to_blob<int>("a123:ffff"), ::testing::ElementsAre());
}